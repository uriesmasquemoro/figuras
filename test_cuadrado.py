import unittest

from math import pi
from cuadrado import Cuadrado

class TestCircle(unittest.TestCase):
    def test_get_area(self):
        c = Cuadrado(3)
        self.assertAlmostEqual(c.get_area(), 3 ** 2)

    def test_get_perimetro(self):
        c = Cuadrado(3)
        self.assertAlmostEqual(c.get_perimetro(), 3 * 4)

if __name__ == '__main__':
    unittest.main()