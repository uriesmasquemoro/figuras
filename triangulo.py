from math import pi
from figura import Figura

class Triangulo(Figura):
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura

    def get_area(self):
        return self.base * self.altura / 2

    def get_perimetro(self):
        return self.base * 3