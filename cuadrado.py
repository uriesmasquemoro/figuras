from math import pi
from figura import Figura

class Cuadrado(Figura):
    def __init__(self, lado = 0):
        self.lado = lado

    def get_area(self):
        return self.lado ** 2

    def get_perimetro(self):
        return self.lado * 4