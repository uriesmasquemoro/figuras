import unittest

from math import pi
from triangulo import Triangulo

class TestCircle(unittest.TestCase):
    def test_get_area(self):
        c = Triangulo(3, 4)
        self.assertAlmostEqual(c.get_area(), 3 * 4 / 2)

    def test_get_perimetro(self):
        c = Triangulo(3, 4)
        self.assertAlmostEqual(c.get_perimetro(), 3 * 3)

if __name__ == '__main__':
    unittest.main()