import unittest

from math import pi
from circulo import Circulo

class TestCircle(unittest.TestCase):
    def test_get_area(self):
        c = Circulo(2)
        self.assertAlmostEqual(c.get_area(), pi * 2 ** 2)

    def test_get_perimetro(self):
        c = Circulo(2)
        self.assertAlmostEqual(c.get_perimetro(), 2 * (pi * 2))

if __name__ == '__main__':
    unittest.main()