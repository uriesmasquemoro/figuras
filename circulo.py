from math import pi
from figura import Figura

class Circulo(Figura):
    def __init__(self, radio = 0):
        self.radio = radio

    def get_area(self):
        return pi * self.radio ** 2

    def get_perimetro(self):
        return 2 * (pi * self.radio)