from abc import ABCMeta, abstractmethod


class Figura:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_area(self):
        raise NotImplementedError

    @abstractmethod
    def get_perimetro(self):
        raise NotImplementedError
